#!/bin/bash

pushd /workdir/build/phyboard-polis-imx8mm-wic/tmp/deploy/images/phyboard-polis-imx8mm

SOURCE_1="core-image-base-phyboard-polis-imx8mm*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/phyboard-polis-imx8mm-wic/tmp/deploy/images/phyboard-polis-imx8mm"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
