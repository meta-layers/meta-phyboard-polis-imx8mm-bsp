
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-base
    core-image-minimal-virt-docker-ce
    core-image-minimal-virt-docker-ce-app-phoronix

You can also run generated qemu images with a command like 'runqemu qemux86'
