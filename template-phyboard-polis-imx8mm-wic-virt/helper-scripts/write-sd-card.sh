#DEVICE="/dev/sdd"
DEVICE="/dev/sdk"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200528055521.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200530150758.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200530174127.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200530184000.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200531110052.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200531115158.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200531163039.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200531182937.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200531191146.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200601152246.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200601222535.rootfs.wic"
#IMAGE="core-image-minimal-virt-docker-ce-imx6q-phytec-mira-rdk-nand-20200601223653.rootfs.wic"
#IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200602094147.rootfs.wic"
IMAGE="core-image-minimal-imx6q-phytec-mira-rdk-nand-20200602111703.rootfs.wic"

E2FSCK="/home/student/projects/e2fsprogs-1.45.6/e2fsck/e2fsck"
RESIZE2FS="/home/student/projects/e2fsprogs-1.45.6/resize/resize2fs"

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "+ sudo ../../../../../bmap-tools/bmaptool copy ${IMAGE} ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo ../../../../../bmap-tools/bmaptool copy ${IMAGE} ${DEVICE}

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "+ sudo parted -l"
sudo parted -l

echo "which partition number do you want to change?"
read PARTNUM

if [[ "${PARTNUM}" = "none" ]];
then
   echo "+ OK no resizing"
   exit 1
fi

if ! [[ "${PARTNUM}" =~ ^[0-9]+$ ]];
then
        echo "Sorry integers only"
        echo "which partition number do you want to change?"
        read PARTNUM
fi

echo "adjusting sdcard's partitions"
set +e
command="sudo partprobe $DEVICE"
echo "$command"; $command
sudo udevadm settle
command="sudo ${E2FSCK} -f ${DEVICE}${PARTNUM}"
echo "$command"; $command
command="sudo parted ${DEVICE} resizepart ${PARTNUM} '90%'"
echo "$command"; $command
sudo udevadm settle
command="sudo ${RESIZE2FS} ${DEVICE}${PARTNUM}"
echo "$command"; $command
sudo udevadm settle
command="sudo ${E2FSCK} -f ${DEVICE}${PARTNUM}"
echo "$command"; $command
set -e
echo "+ sync"
sync

echo "+ sudo fdisk -l ${DEVICE}"
echo "press <ENTER> to go on"
read r
sudo fdisk -l ${DEVICE}

echo "sudo parted -l "
sudo parted -l

#if $IS_SD_CARD; then
#    printWarning "adjusting sdcard's partitions"
#    set +e
#    command="sudo partprobe /dev/$DEVICE"
#    echo "$command"; $command
#    sudo udevadm settle
#    command="sudo e2fsck -f /dev/${DEVICE}2"
#    echo "$command"; $command
#    command="sudo parted /dev/${DEVICE} resizepart 2 '100%'"
#    echo "$command"; $command
#    sudo udevadm settle
#    command="sudo resize2fs /dev/${DEVICE}2"
#    echo "$command"; $command
#    set -e
#
#    sync
#fi




#echo "+ which partition number?"
#read partnum

#sudo ./growpart -N /dev/sdd 1


#set -x
#echo "- +" | sfdisk -N ${PARTNUM} ${DEVICE}
#set +x

#sudo growpart /dev/sdd 1
#sudo resize2fs /dev/sdd 1
