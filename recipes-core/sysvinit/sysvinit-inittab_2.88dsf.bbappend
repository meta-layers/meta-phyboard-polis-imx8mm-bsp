# use runlevel 3 instead of 5 and
do_install_append() {
        sed -i 's/^id:5:initdefault/id:3:initdefault/' ${D}${sysconfdir}/inittab
}
