This README file contains information on building the meta-phyboard-polis-imx8mm
BSP layer, and booting the images contained in the /binary directory.
Please see the corresponding sections below for details.

Build breaks because "u-boot-phytec-imx" can not be built
=========================================================
Please note, that you need to 
> bitbake firmware-imx-8m
before you can successfully "bitbake u-boot-phytec-imx"
or pretty much any other recipe which requires u-boot-phytec-imx.

Note
====

At the momentent the SD card/emmc are not detected via the kernel,
so you need to boot over nfs (which I would do anyways for development).
I use tftp/nfs like that in u-boot:

setenv nfsroot "/opt/poky/polis-rootfs"
setenv bootargs "console=$console root=$nfsroot rootwait rw"
setenv image "phyboard-polis-imx8mm/Image"
setenv fdt_file "phyboard-polis-imx8mm/oftree"

setenv ipaddr 192.168.42.73
setenv serverip 192.168.42.1
setenv netmask 255.255.255.0
setenv gatewayip 192.168.42.254

saveenv

Dependencies
============

This layer depends on:

see: template-phyboard-polis-imx8mm-wic

Patches
=======

Please clone this repo and submit pull requests.

Table of Contents
=================

  I. Building the meta-phyboard-polis-imx8mm BSP layer
 II. Booting the images in /binary


I. Building the meta-phyboard-polis-imx8mm BSP layer
========================================

--- replace with specific instructions for your layer ---

In order to build an image with BSP support for a given release, you
need to download the corresponding BSP tarball from the 'Board Support
Package (BSP) Downloads' page of the Yocto Project website.

Having done that, and assuming you extracted the BSP tarball contents
at the top-level of your yocto build tree, you can build a
phyboard-polis-imx8mm image by adding the location of the meta-phyboard-polis-imx8mm
layer to bblayers.conf, along with any other layers needed (to access
common metadata shared between BSPs) e.g.:

  yocto/meta-xxxx \
  yocto/meta-xxxx/meta-phyboard-polis-imx8mm \

To enable the phyboard-polis-imx8mm layer, add the phyboard-polis-imx8mm MACHINE to local.conf:

  MACHINE ?= "phyboard-polis-imx8mm"

You should then be able to build a phyboard-polis-imx8mm image as such:

  $ source oe-init-build-env
  $ bitbake core-image-sato

At the end of a successful build, you should have a live image that
you can boot from a USB flash drive (see instructions on how to do
that below, in the section 'Booting the images from /binary').

As an alternative to downloading the BSP tarball, you can also work
directly from the meta-xxxx git repository.  For each BSP in the
'meta-xxxx' repository, there are multiple branches, one corresponding
to each major release starting with 'laverne' (0.90), in addition to
the latest code which tracks the current master (note that not all
BSPs are present in every release).  Instead of extracting a BSP
tarball at the top level of your yocto build tree, you can
equivalently check out the appropriate branch from the meta-xxxx
repository at the same location.


II. Booting the images in /binary
=================================

--- replace with specific instructions for your platform ---

This BSP contains bootable live images, which can be used to directly
boot Yocto off of a USB flash drive.

Under Linux, insert a USB flash drive.  Assuming the USB flash drive
takes device /dev/sdf, use dd to copy the live image to it.  For
example:

# dd if=core-image-sato-phyboard-polis-imx8mm-20101207053738.hddimg of=/dev/sdf
# sync
# eject /dev/sdf

This should give you a bootable USB flash device.  Insert the device
into a bootable USB socket on the target, and power on.  This should
result in a system booted to the Sato graphical desktop.

If you want a terminal, use the arrows at the top of the UI to move to
different pages of available applications, one of which is named
'Terminal'.  Clicking that should give you a root terminal.

If you want to ssh into the system, you can use the root terminal to
ifconfig the IP address and use that to ssh in.  The root password is
empty, so to log in type 'root' for the user name and hit 'Enter' at
the Password prompt: and you should be in.

----

If you find you're getting corrupt images on the USB (it doesn't show
the syslinux boot: prompt, or the boot: prompt contains strange
characters), try doing this first:

# dd if=/dev/zero of=/dev/sdf bs=1M count=512
