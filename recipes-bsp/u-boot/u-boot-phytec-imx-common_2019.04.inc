DESCRIPTION = "U-Boot for Phytec imx boards"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=30503fd321432fc713238f582193b78e"

PV = "v2019.04-1.1.0-phy5+git${SRCPV}"

SRCREV = "6567aa7d61eae9c6366a272b1a5f84d058f1d755"
SRCBRANCH = "v2019.04_1.1.0-phy"
SRC_URI = "git://git.phytec.de/u-boot-imx;branch=${SRCBRANCH}"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"
PACKAGE_ARCH = "${MACHINE_ARCH}"
