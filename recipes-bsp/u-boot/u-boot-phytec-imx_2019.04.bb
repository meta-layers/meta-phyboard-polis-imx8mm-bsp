require recipes-bsp/u-boot/u-boot.inc
require recipes-bsp/u-boot/u-boot-phytec-imx-common_${PV}.inc

FILESEXTRAPATHS_prepend_mx6 := "${THISDIR}/${PN}/arm:"
FILESEXTRAPATHS_prepend_mx7 := "${THISDIR}/${PN}/arm:"
FILESEXTRAPATHS_prepend_mx8 := "${THISDIR}/${PN}/aarch64:"

DEPENDS += "bison-native"

SRC_URI += "file://fw_env.config"

SRC_URI += "file://0001-Add-target-to-generate-initial-environment.patch"

# --> this funny u-boot from Phytec needs the ddr_firmware binaries in ${S}
# Note: here we copy deployed stuff from another recipe
#       to the source dir of this recipe!
#
# DEPENDS deals with build-time depenencies
# Read my blog post to find out why DEPENDS does not work here
#
# DEPENDS += " \
#           firmware-imx-8m \
#           "
# RDEPENDS is wrong anyhow for what I want it,
# since it deals with runtime dependencies
#
# RDEPENDS_${PN} += "firmware-imx-8m"
#
# this works:
#
do_compile[depends] = "firmware-imx-8m:do_deploy"

do_compile_prepend() {
    bbnote 8MQ/8MM/8MN firmware
    for ddr_firmware in ${DDR_FIRMWARE_NAME}; do
        bbnote "Copy ddr_firmware: ${ddr_firmware} from ${DEPLOY_DIR_IMAGE} -> ${S} "
        cp ${DEPLOY_DIR_IMAGE}/${ddr_firmware}               ${S}
    done
}
# <-- this funny u-boot from Phytec needs the ddr_firmware binaries in ${S}

PROVIDES += "u-boot"

BOOT_TOOLS = "imx-boot-tools"

do_deploy_append_mx8 () {
	install -d ${DEPLOYDIR}/${BOOT_TOOLS}
	install -m 0777 ${B}/${config}/arch/arm/dts/${UBOOT_DTB_NAME}  ${DEPLOYDIR}/${BOOT_TOOLS}
	install -m 0777 ${B}/${config}/tools/mkimage  ${DEPLOYDIR}/${BOOT_TOOLS}/mkimage_uboot
	install -m 0777 ${B}/${config}/u-boot-nodtb.bin  ${DEPLOYDIR}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${UBOOT_CONFIG}
}

COMPATIBLE_MACHINE = "(phyboard-polis-imx8mm)"
