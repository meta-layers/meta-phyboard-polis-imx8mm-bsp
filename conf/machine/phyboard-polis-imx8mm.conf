#@TYPE: Machine
#@NAME: phyboard-polis-imx8mm

#@DESCRIPTION: Machine configuration for phyboard-polis-imx8mm systems

MACHINEOVERRIDES =. "mx8:mx8m:mx8mm:"


# --> different settings
DEFAULTTUNE_mx8mm  = "armv8a-crc-crypto"
# <-- different settings
# --> this is the default from nxp
require conf/machine/include/imx-base.inc
# we need this specific compiler setting for crypto crap
#require conf/machine/include/tune-cortexa53.inc
# <-- this is the default from nxp
# --> different settings
require conf/machine/include/arm/arch-armv8a.inc
# <-- different settings

# the default MACHINE_EXTRA_RRECOMMENDS
# would not install the kernel modules on the core-image-minimal rootfs
# we could also build a core-image-basic to get them included or:
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += " kernel-modules kernel-devicetree"

# --> add (Linux) firmware
# this is from poky:
MACHINE_FIRMWARE_append = " linux-firmware"

# this adds the above to the image:
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += "${MACHINE_FIRMWARE}"
# <-- add (Linux) firmware

# --> IMX_DEFAULT_BSP
# this is needed to build imx-boot!
IMX_DEFAULT_BSP = "nxp"
# <-- IMX_DEFAULT_BSP

# Kernel configuration
#PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto-custom"

# kernel version
KERNEL_VERSION_PATCHLEVEL="5.8"
KERNEL_SUBLEVEL="5"
PREFERRED_VERSION_linux-yocto-custom = "${KERNEL_VERSION_PATCHLEVEL}.${KERNEL_SUBLEVEL}%"

KERNEL_DEVICETREE += "freescale/imx8mm-phyboard-polis-rdk.dtb"
IMAGE_BOOT_FILES += "oftree"

IMAGE_FSTYPES += "tar.bz2"

KERNEL_IMAGETYPE = "Image"
RDEPENDS_${KERNEL_PACKAGE_NAME}-base = ""

# --> up/standard/debug/virt
ML_DEFAULT_KERNEL := "linux-yocto-custom"
KTYPE ?= "std"
KMACHINE = "arm64-ml"

# If you do not specify a LINUX_KERNEL_TYPE,
# it defaults to "standard"
LINUX_KERNEL_TYPE ?= "${KTYPE}"
# <-- up/standard/debug/virt

# default kernel provider
PREFERRED_PROVIDER_virtual/kernel ??= "${ML_DEFAULT_KERNEL}"

# kernel packages - we can bitbake more than one
KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-up = "up-linux"
KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-std = "std-linux"
#KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-debug = "debug-linux"
KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-virt = "virt-linux"
#KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-xenomai = "xenomai-linux"
#KERNEL_PACKAGE_NAME_pn-linux-yocto-custom-prt = "prt-linux"

# U-Boot configuration
PREFERRED_PROVIDER_u-boot ??= "u-boot-phytec-imx"
PREFERRED_PROVIDER_virtual/bootloader ??= "u-boot-phytec-imx"

PREFERRED_PROVIDER_imx-atf ??= "imx-atf-boundary"
SPL_BINARY = "spl/u-boot-spl.bin"
# Set u-boot DTB
UBOOT_DTB_NAME = "phycore-imx8mm.dtb"

#UBOOT_CONFIG ??= "fspi sd"
#UBOOT_CONFIG[sd] = "phycore-imx8mm_defconfig,sdcard"
#UBOOT_CONFIG[fspi] = "phycore-imx8mm_fspi_defconfig"
#UBOOT_CONFIG[mfgtool] = "phycore-imx8mm_defconfig"
#SPL_BINARY = "spl/u-boot-spl.bin"

UBOOT_MAKE_TARGET = ""
UBOOT_SUFFIX = "bin"
UBOOT_CONFIG ??= "sd"
UBOOT_CONFIG[sd] = "phycore-imx8mm_defconfig,sdcard"

IMAGE_BOOTLOADER = "imx-boot"

IMXBOOT_TARGETS = "flash_evk"

# Set DDR FIRMWARE
DDR_FIRMWARE_NAME = "\
    lpddr4_pmu_train_1d_imem.bin \
    lpddr4_pmu_train_1d_dmem.bin \
    lpddr4_pmu_train_2d_imem.bin \
    lpddr4_pmu_train_2d_dmem.bin \
"

# wic support
IMX_BOOT_SEEK = "33"
WKS_FILE = "imx-imx-boot-bootpart.wks.in"

SERIAL_CONSOLES = "115200;ttymxc2"

MACHINE_FEATURES = "usbgadget usbhost vfat "
MACHINE_FEATURES += " emmc pci wifi bluetooth"
