# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

KTYPE="virt"

require recipes-kernel/linux/linux-yocto-custom_${PV}.inc
require recipes-kernel/linux/linux-yocto-custom-common_5.8.inc

# Skip processing of this recipe if it is not explicitly specified as the
# PREFERRED_PROVIDER for virtual/kernel. This avoids errors when trying
# to build multiple virtual/kernel providers, e.g. as dependency of
# core-image-rt-sdk, core-image-rt.
#python () {
#    if d.getVar("KERNEL_PACKAGE_NAME") == "kernel" and d.getVar("PREFERRED_PROVIDER_virtual/kernel") != "linux-yocto-custom-virt":
#        raise bb.parse.SkipRecipe("Set PREFERRED_PROVIDER_virtual/kernel to linux-yocto-custom-virt to enable it")
#}

