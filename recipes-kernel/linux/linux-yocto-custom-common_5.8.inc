# virtualization?
KERNEL_MODULE_AUTOLOAD += "openvswitch"
# not available anymore?
KERNEL_MODULE_AUTOLOAD += "nf_conntrack_ipv6"

# --> EW-7811UN USB/Wifi dongle
# USB realtek wifi
#
# in this file:
# config/multi-v7-ml-base/features-collection/std-collection.scc
#
# we could do it like that:
# include features/rtl8xxxu/rtl8xxxu.scc
#
# but I prefer to do it conditionally, which does not work in the .scc file above
#
# so in
# recipes-kernel/linux/linux-yocto-custom-common_${KERNEL_VERSION_PATCHLEVEL}.inc
# I do 
# 
# conditionally include kernel fragment:
# include ${@ bb.utils.contains("MACHINE_FEATURES", "wifi", "features/rtl8xxxu/rtl8xxxu.scc" , "", d)}
KERNEL_FEATURES_append = " ${@bb.utils.contains('MACHINE_FEATURES', 'wifi', ' features/rtl8xxxu/rtl8xxxu.scc', '', d)}"
# which seems to work
#
# conditionally automatically load the kernel module
KERNEL_MODULE_AUTOLOAD += " ${@bb.utils.contains("MACHINE_FEATURES", "wifi", "rtl8xxxu", "",d)}"
#
# make sure 'wifi' is also set in DISTRO_FEATURES to install various extra things
# and make wifi work out of the box
#
# <-- EW-7811UN USB/Wifi dongle

# --> imx-sdma .ko as a module (=m) instead of statically linked (=y)
# why? 
# this allows us to load the firmware from the rootfs
#
# in this file:
# config/multi-v7-ml-base/features-collection/std-collection.scc
#
# we could do it like that:
# include features/imx-sdma/imx-sdma.scc
# which only sets CONFIG_IMX_SDMA=m 
# !!!! is ignored !!!! - no idea why ?????
#
# so in
# recipes-kernel/linux/linux-yocto-custom-common_${KERNEL_VERSION_PATCHLEVEL}.inc
# I do
KERNEL_FEATURES_append = " features/imx-sdma/imx-sdma.scc"
# which seems to work
# no idea why it does not work here ?????
#
# <-- imx-sdma .ko as a module (=m) instead of statically linked (=y)

# --> br_netfilter
# bridge: filtering via arp/ip/ip6tables is no longer available by default. 
# Update your scripts to load br_netfilter if you need this.
KERNEL_MODULE_AUTOLOAD += " ${@bb.utils.contains("DISTRO_FEATURES", "virtualization", "br_netfilter", "",d)}"
# <-- br_netfiler
