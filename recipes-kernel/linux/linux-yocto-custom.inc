# This is what should be common to all kernel versions
# This file was derived from the linux-yocto-custom.bb recipe in
# oe-core.
#
# linux-yocto-custom.bb:
#
#   A yocto-bsp-generated kernel recipe that uses the linux-yocto and
#   oe-core kernel classes to apply a subset of yocto kernel
#   management to git managed kernel repositories.
#
# Warning:
#
#   Building this kernel without providing a defconfig or BSP
#   configuration will result in build or boot errors. This is not a
#   bug.
#
# Notes:
#
#   patches: patches can be merged into to the source git tree itself,
#            added via the SRC_URI, or controlled via a BSP
#            configuration.
#
#   example configuration addition:
#            SRC_URI += "file://smp.cfg"
#   example patch addition:
#            SRC_URI += "file://0001-linux-version-tweak.patch
#   example feature addition:
#            SRC_URI += "file://feature.scc"
#

# This version extension should match CONFIG_LOCALVERSION in defconfig
LINUX_VERSION_EXTENSION ?= "-custom-ml-${KTYPE}"
PV = "${LINUX_VERSION}${LINUX_VERSION_EXTENSION}+git${SRCPV}"

SRC_URI += "\
           git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH};name=the-kernel \
           file://arm64-ml-common;type=kmeta;destsuffix=arm64-ml-common \
           "

# in case we wanted to get it directly via gitpod:
#SRC_URI += "\
#           git://gp@gitpod.res.training/linux-stable.git;protocol=git;branch=${KBRANCH};user=gp:gp \
#           file://arm64-ml-common;type=kmeta;destsuffix=arm64-ml-common \
#           "

SRCREV_machine ?= "${SRCREV}"

require recipes-kernel/linux/linux-yocto.inc

DESCRIPTION = "Mainline kernel"

require linux-ml-configs.inc
require linux-ml-machines.inc

# oftree for phytec bootloader
require linux-common-phytec.inc

# Let's try an in-tree defconfig:
KERNEL_DEFCONFIG_phyboard-polis-imx8mm ?= "defconfig"
KBUILD_DEFCONFIG_phyboard-polis-imx8mm ?= "defconfig"

KCONFIG_MODE="--alldefconfig"
