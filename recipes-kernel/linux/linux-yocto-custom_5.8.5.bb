# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc
# we don't want this included everywhere, so move it furter up
#require recipes-kernel/linux/linux-yocto-custom-common_5.4.inc

KBRANCH = "linux-5.8.y"

LINUX_VERSION = "5.8.5"

# some 5.8.x LIC_FILE_CHECKSUM:
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-08-27 09:31:49 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-08-27 09:31:49 +0200
# commit	9ece50d8a470ca7235ffd6ac0f9c5f0f201fe2c8 (patch)
# tree		186a768db5fe6c9a12fa76a1710fa16c99f1257e
# parent	c05dea7c0f3ff1a8d63b41542c4161406093dc2f (diff)
# download	linux-9ece50d8a470ca7235ffd6ac0f9c5f0f201fe2c8.tar.gz
# Linux 5.8.5 v5.8.5 linux-5.8.y

SRCREV ?= "9ece50d8a470ca7235ffd6ac0f9c5f0f201fe2c8"

PATCHPATH="${THISDIR}/patch/5.8.x"

FILESEXTRAPATHS_prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://arm64-ml-user-patches.scc \
           "
SRC_URI_append += " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "
