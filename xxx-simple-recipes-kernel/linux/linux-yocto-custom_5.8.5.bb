# This file was derived from the linux-yocto-custom.bb recipe in
# oe-core.
#
# linux-yocto-custom.bb:
#
#   A yocto-bsp-generated kernel recipe that uses the linux-yocto and
#   oe-core kernel classes to apply a subset of yocto kernel
#   management to git managed kernel repositories.
#
# Warning:
#
#   Building this kernel without providing a defconfig or BSP
#   configuration will result in build or boot errors. This is not a
#   bug.
#
# Notes:
#
#   patches: patches can be merged into to the source git tree itself,
#            added via the SRC_URI, or controlled via a BSP
#            configuration.
#
#   example configuration addition:
#            SRC_URI += "file://smp.cfg"
#   example patch addition:
#            SRC_URI += "file://0001-linux-version-tweak.patch
#   example feature addition:
#            SRC_URI += "file://feature.scc"
#

inherit kernel
require recipes-kernel/linux/linux-yocto.inc
require linux-common-phytec.inc

# some 5.8.x LIC_FILE_CHECKSUM:
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

#SRC_URI = "git://git.phytec.de/linux-imx.git;protocol=git;bareclone=1;branch=${KBRANCH}"
SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git;protocol=git;bareclone=1;branch=${KBRANCH}"

# SRC_URI += "file://defconfig"

SRC_URI += "file://phyboard-polis-imx8mm.scc \
            file://phyboard-polis-imx8mm.cfg \
            file://phyboard-polis-imx8mm-user-config.cfg \
            file://phyboard-polis-imx8mm-user-patches.scc \
            file://phyboard-polis-imx8mm-user-features.scc \
           "

# --> add phytec device tree and dependencies
FILESEXTRAPATHS_prepend := "${THISDIR}/5.8.x/patches:"
SRC_URI += "file://0001-freescale-imx8mm-phyboard-polis-rdk.dts-dependencies.patch"
# <-- add phytec device tree and dependencies

KBRANCH = "linux-5.8.y"

LINUX_VERSION ?= "5.8.5"
LINUX_VERSION_EXTENSION ?= "-stable"

SRCREV="9ece50d8a470ca7235ffd6ac0f9c5f0f201fe2c8"

PV = "${LINUX_VERSION}+git${SRCPV}"

# Let's try an in-tree defconfig:
KERNEL_DEFCONFIG_phyboard-polis-imx8mm ?= "defconfig"
KBUILD_DEFCONFIG_phyboard-polis-imx8mm ?= "defconfig"

KCONFIG_MODE="--alldefconfig"

COMPATIBLE_MACHINE_phyboard-polis-imx8mm = "phyboard-polis-imx8mm"
